package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;

public class TaskTest {

    LocalDate localDate = LocalDate.now();

    // 8. Why is this underlined in red?
    private Task task = new Task(1L, "mop the floors", localDate, false);

    // 7. This test is not compiling - why?
    @Test
    public void creation(){

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

}
