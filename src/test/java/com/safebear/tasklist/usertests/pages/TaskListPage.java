package com.safebear.tasklist.usertests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TaskListPage {

    WebDriver driver;
    WebDriverWait wait;

    // 9. Why can't this find my web element?
    @FindBy(xpath = "//input[@id='addTask']")
    WebElement enterTaskField;
    public TaskListPage(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public void addTask(String taskname){

        enterTaskField.sendKeys(taskname);
        // 10. After typing the task name I now want to press the 'Enter' key in order to submit it. How do I do this?
        enterTaskField.sendKeys(Keys.ENTER);



    }

    public boolean checkForTask(String taskname){

        wait.until(ExpectedConditions.visibilityOf(enterTaskField));

        // 11. Why is this underlined in red?
        return driver.findElements(By.xpath("//span[contains(text(),\"" + taskname + "\")]")).size() !=0;
    }

}
