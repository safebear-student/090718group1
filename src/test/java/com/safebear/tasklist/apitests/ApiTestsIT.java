package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;

public class ApiTestsIT {

    private final String DOMAIN = System.getProperty("domain");
    private final int PORT = Integer.parseInt(System.getProperty("port"));
    private final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        // 2. And why are these not working?
        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;


        RestAssured.registerParser("application/json", Parser.JSON);
    }

    @Test
    public void testTasksEndPoint(){

        // 1. Why is this not compiling?
        get("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

        // 4. The test also seems to fail - why?

    }

}
